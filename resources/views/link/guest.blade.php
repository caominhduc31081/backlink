@extends('layout.master')

@section('container')

    <br />
    <div class="row">
        <div class="col-sm-8 offset-2">
            <div class="card text-center">
                <h3 class="card-header">Khóa nội dung</h3>
                <div class="card-block">
                    
                    <p class="card-text">Để tiếp tục sử dụng bạn vui lòng nhấn nút "KẾT NỐI" </p>
                    <a href="{{url('/facebook')}}" class="btn btn-primary">KẾT NỐI</a>
                </div>
            </div>
        </div>
        
    </div>
@endsection