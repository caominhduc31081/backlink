<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Socialite;
use Auth;
use App\User;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;
    
    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function logout(){
        
        Auth::logout();
        
        return redirect('/');
        
    }
    /**
     * Redirect the user to the Facebook authentication page.
     *
     * @return \Illuminate\Http\Response
     */
    public function redirectToProvider()
    {
        return Socialite::driver('facebook')->redirect();
    }

    /**
     * Obtain the user information from Facebook.
     *
     * @return \Illuminate\Http\Response
     */
    public function handleProviderCallback()
    {   

        $account = Socialite::driver('facebook')->user();
        dd($account);
        dd("https://graph.facebook.com/v2.0/?ids=http://www.facebook.com/$account->id&access_token=$account->token");

        dd($account->id);
        dd($account->avatar);
        
        $user = $this->createOrUpdateToken($account);
        
        Auth::login($user);

        if(Auth::check()){
            
            return redirect()->back();
            
        }
    }


    public function createOrUpdateToken($account){

        $user = User::where('fid',$account->id)->first();

        if(empty($user)){
            $user = User::create([
                'name' => $account->getName(),
                'fid'  => $account->getId(),
                'token'=> $account->access_token,
            ]);
        }else{
            $user->token = $account->token;
            $user->save();
        }

        return $user;
    }
}
