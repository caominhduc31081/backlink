<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\FacebookService;
use App\User;
use Auth;
Use Socialite;

class LinkController extends Controller
{   

    protected function __contruct(){

        if(Auth::guest())
            return redirect('/facebook');
            
    }
    
    public function show($id){
        if(Auth::guest()){
            return view('link.guest');
        }else if($this->check(Auth::user())){
            return view('link.fail');
        }else{
            return view('link.show');
        }
    }

    protected function check($user){
        $this->isMember($user,"443140989452847");
        //check Group Member

        //Check Group Backup Member

    }

    protected function isMember($user,$group_id){
        
        $members = new FacebookService($group_id,'members',['limit' => 10000]);
        
        $members_id = array();

        foreach($members->value as $member){
            array_push($members_id,$member->id);
        }
        //dd($members_id);
        $user_id = Auth::user()->fid;
        dd($user_id);
        dd(array_search($user_id,$members_id));
    }
}
