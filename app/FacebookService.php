<?php

namespace App;

use GuzzleHttp\Client;

class FacebookService {

    private $option = [
        
    ];

    private $access_token = "EAAFSxExDNRYBAAGHSpTKKQpRk8cSfUu0BnE3DVwStq4akLkzGGCuTexBNeRc8lBjEWpAZBENfQiOM4q2yVzFeitumnXXFQDZBypF72Le5uT8FS5AIFLudcjdXXmbGZC82ZCNi3mXxxLTZAr2gZBQlmjo0Vz7XtQQo2oZA4kbVZA9LX2KIZC2fpZAcc1RzkMQaBALMZD";

    private $node;

    private $edge;

    private $uri = "https://graph.facebook.com/v2.12";

    public $value;
    
    public function __construct($node,$edge,$option,$method = 'GET'){

        $this->node = $node;
        $this->edge = $edge;
        $this->option = $option;

        $this->value =  $this->sendRequest($method);
    }
    
    public function getMember($group_id){
        $this->node = $group_id;
        $this->edge = "members";
        $this->option['limit'] = 5000;
        return $this->sendRequest('GET'); 
    }

    protected function addNode($name){
        $this->uri .= "/$name";
    }

    protected function addEdge($name){
        $this->uri .= "/$name";
    }

    protected function addOption($option){
        $this->uri .= "?";
        foreach($option as $key => $value){
            $this->uri .= "$key=$value&";
        }
        $this->uri .= "access_token=$this->access_token";
    }

    protected function createUri(){
        $this->addNode($this->node);
        $this->addEdge($this->edge);
        $this->addOption($this->option);
    }

    public function sendRequest($method){

        
        $this->createUri();

        $client = new Client();

        $res = $client->request($method,$this->uri,[
            'headers' => [
                'Accept' => 'application/json',
            ],
        ]);
        
        $data = json_decode($res->getBody());

        return $data->data;
    }
}



?>